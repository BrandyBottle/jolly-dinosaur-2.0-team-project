﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class InteractableObject : MonoBehaviour {


    public enum ColliderType { Box, Sphere };

    public ColliderType colliderType;
    public Text uiText;
    public string uiName;
    [HideInInspector] public bool triggered;

    void Awake()
    { 
        switch(colliderType)
        {
            case ColliderType.Box:
                BoxCollider bc = gameObject.AddComponent<BoxCollider>() as BoxCollider;
                break;

            case ColliderType.Sphere:
                SphereCollider sc = gameObject.AddComponent<SphereCollider>() as SphereCollider;
                break;

            default:
                BoxCollider def = gameObject.AddComponent<BoxCollider>() as BoxCollider;
                break;
        }
    }

    public void triggeredObject()
    {
        uiText.text = "Pick up " + this.uiName;
        triggered = true;
    }

    public void untriggeredObject()
    {
        triggered = false;
        uiText.text = " ";
    }

}
