﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]

public class ThrowableObject : InteractableObject {

    bool holding;
    [HideInInspector] public Transform playerPosition;
    [HideInInspector] public Transform parentCamera;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (holding)
        {
            holdObject();

            // Right Click
            if (Input.GetMouseButtonDown(1))
            {
                throwObject();
            }
        }
    }

    void OnMouseDown()
    {
        float distance = Vector3.Distance(this.transform.position, playerPosition.transform.position);
        if (distance <=  2.55f && !holding && triggered)
        {
            holding = true;
            this.uiText.enabled = false;
            this.GetComponent<Rigidbody>().useGravity = false;
            this.GetComponent<Rigidbody>().detectCollisions = true; 
        }
    }

    void holdObject()
    {
        this.GetComponent<Rigidbody>().velocity = Vector3.zero;
        this.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        this.transform.SetParent(parentCamera);
    }

    void throwObject()
    {
        this.GetComponent<Rigidbody>().AddForce(parentCamera.forward * 600);
        this.GetComponent<Rigidbody>().useGravity = true;
        this.transform.SetParent(null);
        holding = false;
        this.uiText.enabled = true;
    }
}
