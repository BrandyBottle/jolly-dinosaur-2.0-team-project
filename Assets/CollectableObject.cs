﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class CollectableObject : InteractableObject
{

    public Transform playerPosition;

    void OnMouseDown()
    {
        float distance = Vector3.Distance(this.transform.position, playerPosition.transform.position);
        if (distance <= 2.55f && triggered)
        {
            // Collect the object
            gameObject.SetActive(false);

            // TODO Add the object to an inventory
        }
    }
}
