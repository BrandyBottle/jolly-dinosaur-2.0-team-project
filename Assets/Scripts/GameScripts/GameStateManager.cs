﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class GameStateManager : MonoBehaviour {

    private bool gameWon = false;
    private bool gameLost = false;

	public Text uiText;

    void Start()
    {
        // WIP, subscribe to other event listeners... Win state etc....
        PlayerDamage.onLose += loseState;
    }

    public void winState()
    {
        if(!gameWon)
        {
            gameWon = true;
			//uiText.text = "Collected CA and Escaped!";
			//uiText.enabled = true;
			//Debug.Log(uiText.text);
        }
    }

    public void loseState()
    {
        if (!gameLost)
        {
            gameLost = true;
            // Go to Game Over Scene
        }
    }
}
