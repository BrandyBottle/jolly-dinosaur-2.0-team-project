﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchLight : MonoBehaviour {

    int batteryLife;
    float timer;

	// Use this for initialization
	void Start ()
    {
        batteryLife = 4;
        

        // Start the game with the torch light disabled
        GetComponent<Light>().enabled = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        
        if(GetComponent<Light>().enabled)
        {
            timer += Time.deltaTime;
            if(timer > 5f)
            {
                timer = 0f;
                batteryLife--;
                Debug.Log("TIMER");
                if (batteryLife <= 0)
                {
                    GetComponent<Light>().enabled = false;
                    Debug.Log("DEAD");
                }
            }
            
        }
		if(Input.GetKeyDown("space") || Input.GetKeyDown("joystick button 3"))
        {
            GetComponent<Light>().enabled = !GetComponent<Light>().enabled;
        }
	}
}
