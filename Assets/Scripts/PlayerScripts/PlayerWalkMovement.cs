﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class PlayerWalkMovement : MonoBehaviour
{

    [SerializeField] private string horizontalMovement;
    [SerializeField] private string verticalMovement;
    [SerializeField] private float walkSpeed; // Maybe add run, crouch and sneak speed?

    private CharacterController playerController;

	// Use this for initialization
	void Awake ()
    {
        playerController = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        movePlayer(getSpeed());
	}

    void movePlayer(float speed)
    {
        float horizontalMove = Input.GetAxis(horizontalMovement) * speed;
        float verticalMove = Input.GetAxis(verticalMovement) * speed;

        // Apply the movement
        Vector3 forward = transform.forward * verticalMove;
        Vector3 right = transform.right * horizontalMove;

        playerController.SimpleMove(forward + right);
    }

    // Checks what movement state the player is and returns relevant movement speed 
    // i.e walk, crouch, sneak all have different speeds
    // Does nothing for the time and being besides return the walkSpeed
    float getSpeed()
    {
        return walkSpeed;
    }
}
