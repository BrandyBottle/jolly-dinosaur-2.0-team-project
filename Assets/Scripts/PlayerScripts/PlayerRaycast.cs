﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRaycast : MonoBehaviour {

    bool Key;
    InteractableObject objectHit;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Ray ray = this.GetComponent<Camera>().ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 2.5f))
        { 
            if (hit.collider.tag == "Interactable")
            {
                objectHit = hit.collider.gameObject.GetComponent<InteractableObject>();
                objectHit.triggeredObject();
                Key = true;
            }
            else
            {
                // Hitting something else.
                Key = false;
                if (objectHit != null)
                {
                    objectHit.untriggeredObject();
                    objectHit = null;
                }
            }
        }
        else if (Key == true)
        {
            // not anymore.
            Key = false;
            objectHit.untriggeredObject();
            objectHit = null;
        }
        //Debug.DrawRay(ray.origin, ray.direction * 1000, Color.yellow);
    }
}
