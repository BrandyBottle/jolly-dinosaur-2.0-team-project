﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDamage : MonoBehaviour {

    int hitpoints = 2;

    public delegate void LoseAction();
    public static event LoseAction onLose;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(hitpoints <= 0)
        {
            onLose();
        }
		
	}

    void takeHit()
    {
        hitpoints--;
    }
}
