﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour {

	void OnTriggerEnter(Collider hit)
    {
        if(hit.tag == "Collectable")
        {
            FindObjectOfType<PlayerInventory>().getKey();
            Destroy(hit.gameObject);
        }
        else if(hit.tag == "Finish" && FindObjectOfType<PlayerInventory>().hasKey)
        {
            FindObjectOfType<GameStateManager>().winState();
        }
    }
}
