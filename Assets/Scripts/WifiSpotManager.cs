﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

using UnityEngine;

public class WifiSpotManager : MonoBehaviour {

    WifiSpot[] wifiSpots;
    WifiSpot currentActiveWifiSpot;
    public Text distanceText;
    public Transform playerPosition;

    float distance;

	// Use this for initialization
	void Start () {
        wifiSpots = FindObjectsOfType<WifiSpot>();
        activateFirstSpot();
    }
	
	// Update is called once per frame
	void Update () {
        if(currentActiveWifiSpot.triggered)
        {
            Debug.Log("Wifi Hit");
            spotTriggered();
        }
        calculateDistance();
        distanceText.text = "Distance: " + (int)distance;
	}

    // Randomly selects one of the available Wifi Spots and activates it
    void activateFirstSpot()
    {
        int numberOfSpots = wifiSpots.Length;

        int selectedWifiSpot = Random.Range(0, numberOfSpots);

        currentActiveWifiSpot = wifiSpots[selectedWifiSpot];

        currentActiveWifiSpot.activate();
    }

    void activateNewSpot()
    {
        int numberOfSpots = wifiSpots.Length;
        int selectedWifiSpot = 0;
        WifiSpot tempWifiSpot = currentActiveWifiSpot;

        // Prevents the same spot being activated again in succession
        while (currentActiveWifiSpot == tempWifiSpot)
        {
            selectedWifiSpot = Random.Range(0, numberOfSpots);
            tempWifiSpot = wifiSpots[selectedWifiSpot];
        }

        currentActiveWifiSpot = wifiSpots[selectedWifiSpot];

        currentActiveWifiSpot.activate();
    }


    // When a Wifi Spot is triggered, set a new one.
    public void spotTriggered()
    {
        currentActiveWifiSpot.deactivate();
        activateNewSpot();
    }

    void calculateDistance()
    {
        distance = Vector3.Distance(currentActiveWifiSpot.transform.position, playerPosition.position);
    }
}
